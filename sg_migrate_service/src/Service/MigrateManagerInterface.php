<?php

namespace Drupal\sg_migrate_service\Service;

/**
 * Interface MigrateManagerInterface
 *
 * @package Drupal\sg_migrate_service\Service
 */
interface MigrateManagerInterface {

  /**
   * @param bool $update
   * @param bool $sync
   * @param int $limit
   * @param bool $batch
   *
   * @return bool
   */
  public function process(bool $batch = TRUE, bool $update = TRUE, bool $sync = TRUE, int $limit = 0): bool;

  /**
   * @return array
   */
  public function getMigrationIds(): array;

  /**
   * @param array $ids
   *
   * @return void
   */
  public function setMigrationIds(array $ids);
}
