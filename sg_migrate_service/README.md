# Example

## Instantiate a migrate service

```php

// Migrate manager
/**
 * @var $migrateManager Drupal\migrate_manager\Service\MigrateManager
 */
$migrateManager = \Drupal::service('sgdev.sg_migrate_service.manager');

```

## Usage

```php

// Set migrate id list.
$migrateManager->setMigrationIds(['test_article_node_csv_import']);

$migrateManager->process();
```
