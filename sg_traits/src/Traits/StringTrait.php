<?php

namespace Drupal\sg_traits\Traits;

/**
 * Trait StringTrait
 *
 * @package Drupal\sg_traits\Traits
 */
trait StringTrait {

  /**
   * @param string $subject
   * @param string $string
   * @param string $prefix
   * @param string $suffix
   *
   * @return string
   *
   */
  public function replaceKeepCase(string $subject, string $string, string $prefix = '', string $suffix = ''): string {
    $index = 0;
    $resultstring = "";
    while ($index < strlen($subject)) {
      if (strtoupper(substr($subject, $index, strlen($string))) == strtoupper($string)) {
        $resultstring .= $prefix . substr($subject, $index, strlen($string)) . $suffix;
        $index = $index + strlen($string);
      }
      else {
        $resultstring .= substr($subject, $index, 1);
        $index++;
      }
    }
    return $resultstring;
  }

  /**
   * @param string $string
   * @param int $beforeMaxLength
   * @param int $afterMaxLength
   * @param string $delimiter
   *
   * @return string
   */
  public function shortenText(string $string, int $beforeMaxLength, int $afterMaxLength, string $delimiter = '...'): string {
    if (strlen($string) > ($beforeMaxLength + $beforeMaxLength) * 2) {
      $truncated = substr($string, 0, $beforeMaxLength) . $delimiter . substr($string, -1 * $afterMaxLength);
    }
    else {
      $truncated = $string;
    }
    return $truncated;
  }

  /**
   * @param string $text
   * @param int $maxLength
   * @param string $beforeTextIdentifier
   * @param string $afterTextIdentifier
   *
   * @return string
   */
  public function truncateArround(string $text, int $maxLength, string $beforeTextIdentifier = '<strong>', string $afterTextIdentifier = '</strong>'): string {

    $beforePos = strpos($text, $beforeTextIdentifier);
    $afterPos = strpos($text, $afterTextIdentifier);
    $before = substr($text, 0, $beforePos);
    $after = substr($text, $afterPos + strlen($afterTextIdentifier));
    $highlightPart = explode($beforeTextIdentifier, $text);
    $highlightText = !empty($highlightPart[1]) ? explode($afterTextIdentifier, $highlightPart[1])[0] : '';

    $length = ($maxLength - strlen($highlightText)) / 2;
    if (strlen($before) > $length) {
      $beforeTruncated = '...' . substr($before, strlen($before) - $length);
    }
    else {
      $beforeTruncated = $before;
    }
    if (strlen($after) > $length) {
      $afterTruncated = substr($after, 0, $length) . '...';
    }
    else {
      $afterTruncated = $after;
    }

    return $beforeTruncated . $beforeTextIdentifier . $highlightText . $afterTextIdentifier . $afterTruncated;

  }

}
