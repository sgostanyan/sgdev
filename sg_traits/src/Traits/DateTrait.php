<?php

namespace Drupal\sg_traits\Traits;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Trait DateTrait
 *
 * @package Drupal\sg_traits\Traits
 */
trait DateTrait {

  /**
   * @param $startDate
   * @param $endDate
   * @param $format
   *
   * @return array
   */
  public function getBetweenDates($startDate, $endDate, $format = NULL): array {

    $rangArray = [];

    $startDate = strtotime($startDate);
    $endDate = strtotime($endDate);

    for ($currentDate = $startDate; $currentDate <= $endDate; $currentDate += (86400)) {
      $date = date($format ?? 'Y-m-d', $currentDate);
      $rangArray[] = $format ? $date : strtotime($date);
    }

    return $rangArray;
  }

  /**
   * @param $string
   *
   * @return bool
   */
  public function isTimestamp($string): bool {
    try {
      new \DateTime('@' . $string);
    }
    catch (\Exception $e) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @param string $timezone
   *  Ex : date_default_timezone_get()
   *
   * @return string
   * @throws \Exception
   */
  public function getCurrentDate(string $timezone = DateTimeItemInterface::STORAGE_TIMEZONE): string {
    $date = new DrupalDateTime('now');
    $date->setTimezone(new \DateTimezone($timezone));
    return $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
  }

}
