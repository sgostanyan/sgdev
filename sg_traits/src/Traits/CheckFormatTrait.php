<?php

namespace Drupal\sg_traits\Traits;

/**
 * Trait CheckFormatTrait
 *
 * @package Drupal\sg_traits\Traits
 */
trait CheckFormatTrait {

  /**
   * @param $tvaNumber
   *
   * @return bool
   */
  public function isTvaNumber($tvaNumber): bool {
    return !empty(preg_match('/^(AT|BE|BG|CY|CZ|DE|DK|EE|EL|ES|FI|FR|HR|HU|IE|IT|LT|LU|LV|MT|NL|PL|PT|RO|SE|SI|SK)[0-9A-Za-z\.\-\+\*\~]{2,15}$/', $tvaNumber));
  }

  /**
   * @param $siret
   *
   * @return bool
   */
  public function isSiret($siret): bool {

    if (strlen($siret) != 14) {
      return FALSE;
    }
    if (!is_numeric($siret)) {
      return FALSE;
    }

    for ($index = 0; $index < 14; $index++) {
      $number = (int) $siret[$index];
      if (($index % 2) == 0) {
        if (($number *= 2) > 9) {
          $number -= 9;
        }
      }
      $sum += $number;
    }

    return !($sum % 10) != 0;
  }

  /**
   * @param $siren
   *
   * @return bool
   */
  public function isSiren($siren): bool {
    if (strlen($siren) != 9) {
      return FALSE;
    }
    if (!is_numeric($siren)) {
      return FALSE;
    }

    for ($index = 0; $index < 9; $index++) {
      $number = (int) $siren[$index];
      if (($index % 2) != 0) {
        if (($number *= 2) > 9) {
          $number -= 9;
        }
      }
      $sum += $number;
    }
    return !($sum % 10) != 0;
  }

  /**
   * @param $ean13
   *
   * @return bool
   */
  public function isEan13($ean13): bool {
    if (strlen($ean13) != 13) {
      return FALSE;
    }
    if (!is_numeric($ean13)) {
      return FALSE;
    }

    for ($index = 0; $index < 12; $index++) {
      $number = (int) $ean13[$index];
      if (($index % 2) != 0) {
        $number *= 3;
      }
      $sum += $number;
    }

    $key = $ean13[12];

    return !(10 - ($sum % 10) != $key);
  }

}
