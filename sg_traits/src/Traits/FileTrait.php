<?php

namespace Drupal\sg_traits\Traits;

/**
 * Trait FileTrait
 *
 * @package Drupal\sg_traits\Traits
 */
trait FileTrait {

  /**
   * @param $size
   *
   * @return array|string|string[]
   */
  public function formatSize($size): array|string {
    $ko = round($size / 1000, 2);
    return str_replace('.', '', $ko < 1000 ? $ko . ' Ko' : $ko / 1000 . ' Mo');
  }

  /**
   * @param string $data
   * @param string $destinationFolder
   * @param string $filename
   * @param int $folderChmod
   *
   * @return null
   */
  public function createFileEntityFromBase64(string $data, string $destinationFolder, string $filename, int $folderChmod = 777) {

    $fileSystem = \Drupal::service('file_system');
    $fileRepository = \Drupal::service('file.repository');

    mkdir($destinationFolder);
    $fileSystem->prepareDirectory($directory, $fileSystem::CREATE_DIRECTORY | $fileSystem::MODIFY_PERMISSIONS | $fileSystem::CHMOD_DIRECTORY);
    $fileSystem->chmod($destinationFolder, $folderChmod);
    $fileUri = $destinationFolder . '/' . $filename;
    $fileEntity = $fileRepository->writeData($data, $fileUri);

    return $fileEntity->id() ?? NULL;
  }

}