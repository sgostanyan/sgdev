<?php

namespace Drupal\sg_stream_plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface StreamParserInterface
 *
 * @package Drupal\sg_stream_plugin
 */
interface StreamParserInterface extends PluginInspectionInterface {

  /**
   * @return mixed
   */
  function getName();

  /**
   * @param string $data
   *
   * @return mixed
   */
  function prepare(string $data);

  /**
   * @param string $uri
   *
   * @return mixed
   */
  function getData(string $uri);

  /**
   * @return mixed
   */
  function getOptions();

  /**
   * @param $path
   *
   * @return mixed
   */
  function fetch($path);

}
