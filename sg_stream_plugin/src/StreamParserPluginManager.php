<?php

namespace Drupal\sg_stream_plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class StreamParserPluginManager
 *
 * @package Drupal\sg_stream_plugin
 */
class StreamParserPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/StreamParser', $namespaces, $module_handler, 'Drupal\sg_stream_plugin\StreamParserInterface', 'Drupal\sg_stream_plugin\Annotation\StreamParser');
    $this->alterInfo('sg_stream_plugin_parsers_info');
    $this->setCacheBackend($cache_backend, 'sg_stream_parser_plugins');
  }

}
