<?php

namespace Drupal\sg_stream_plugin\Plugin\StreamParser;

use Drupal\sg_stream_plugin\StreamParserPluginBase;

/**
 * Provides a 'csv' parser.
 *
 * @StreamParser(
 *   id = "csv",
 *   name = @Translation("CSV"),
 *   options = {
 *    "separator" = ",",
 *    "enclosure" = """",
 *    "escape" = "\",
 *    "length" = 0
 *   }
 * )
 */
class Csv extends StreamParserPluginBase {

  /**
   * @param $data
   *
   * @return array|false
   */
  public function prepare($data) {
    $options = $this->getOptions();
    if ($data) {
      $rows = [];
      while ($row = fgetcsv($data, $options['length'], $options['separator'], $options['enclosure'], strval($options['escape']))) {
        $rows[] = $row;
      }
      fclose($data);
      return $rows;
    }
    return FALSE;
  }

  /**
   * @param $path
   *
   * @return false|resource
   */
  function fetch($path) {
    return fopen($path, 'r');
  }
}
