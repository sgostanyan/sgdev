<?php

namespace Drupal\sg_stream_plugin\Plugin\StreamParser;

use Drupal\sg_stream_plugin\StreamParserPluginBase;

/**
 * Provides a 'rss' parser.
 *
 * @StreamParser(
 *   id = "rss",
 *   name = @Translation("RSS"),
 *   options={}
 * )
 */
class Rss extends StreamParserPluginBase {

  /**
   * @param $data
   *
   * @return mixed
   */
  public function prepare($data) {
    $json = json_encode($data);
    return json_decode($json, TRUE);
  }

  /**
   * @param $path
   *
   * @return \$1|false|mixed|\SimpleXMLElement
   */
  public function fetch($path) {
    return simplexml_load_string(file_get_contents($path));
  }
}
