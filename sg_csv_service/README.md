# Example

## Instantiate a csv service

```php
// Csv manager
$csvManager = \Drupal::service('sgdev.sg_csv_service.manager');

```

## Usage

```php

$users = Drupal::entityTypeManager()->getStorage('user')->loadMultiple();

/**
 * @var \Drupal\user\Entity\User $user
 */
foreach ($users as $user) {

  $userDataItems = [
    $user->id(),
    $user->getAccountName(),
    $user->getEmail(),
  ];

  /**
   * @var \Drupal\sg_csv_service\manager\Service\CsvManagerInterface $csvManager
   */
  $csvManager->writeCsvFile([$userDataItems], 'fdtest', 'private://import', 1);
    
}
```

