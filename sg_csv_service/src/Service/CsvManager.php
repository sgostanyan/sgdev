<?php

namespace Drupal\sg_csv_service\Service;

use Drupal\Core\File\FileSystemInterface;

/**
 * Class CsvManager
 *
 * @package Drupal\sg_csv_service\Service
 */
class CsvManager implements CsvManagerInterface {

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * CsvManager constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * @param array $rows
   * @param string $filename
   * @param string $directory
   * @param bool $append
   * @param string $separator
   * @param string $enclosure
   *
   * @return bool
   */
  public function writeCsvFile(array $rows, string $filename, string $directory, bool $append = FALSE, string $separator = ',', string $enclosure = '"'): bool {
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::EXISTS_REPLACE);
    $filename = $directory . '/' . $filename . '.csv';

    $mode = 'a+';

    if (!$append && file_exists($filename)) {
      $this->fileSystem->delete($filename);
      $mode = 'w+';
    }

    $csv_file = fopen($filename, $mode);
    if ($csv_file) {
      foreach ($rows as $offset => $record) {
        fputcsv($csv_file, $record, $separator, $enclosure);
      }
      fclose($csv_file);
      return TRUE;
    }
    return FALSE;
  }

}
