<?php

namespace Drupal\sg_csv_service\Service;


/**
 * Interface CsvManagerInterface
 *
 * @package Drupal\sg_csv_service\Service
 */
interface CsvManagerInterface {

  /**
   * @param array $rows
   * @param string $filename
   * @param string $directory
   * @param bool $append
   * @param string $separator
   * @param string $enclosure
   *
   * @return bool
   */
  public function writeCsvFile(array $rows, string $filename, string $directory, bool $append = FALSE, string $separator = ',', string $enclosure = '"'): bool;
}
