<?php

namespace Drupal\sg_batch_plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Interface BatchWorkerCallbackPluginInterface
 *
 * @package Drupal\sg_batch_plugin
 */
interface BatchWorkerCallbackPluginInterface extends PluginInspectionInterface {

  /**
   * @param $params
   * @param $context
   *
   * @return mixed
   */
  static function execute($params, &$context);

  /**
   * @param $success
   * @param $results
   * @param $operations
   *
   * @return mixed
   */
  static function finished($success, $results, $operations);

}
