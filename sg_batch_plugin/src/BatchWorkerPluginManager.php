<?php

namespace Drupal\sg_batch_plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class BatchWorkerPluginManager
 *
 * @package Drupal\sg_batch_plugin
 */
class BatchWorkerPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BatchWorker', $namespaces, $module_handler, 'Drupal\sg_batch_plugin\BatchWorkerPluginInterface', 'Drupal\sg_batch_plugin\Annotation\BatchWorker');
    $this->alterInfo('batch_worker_info');
    $this->setCacheBackend($cache_backend, 'batch_worker_plugins');
  }

}
