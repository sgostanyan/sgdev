<?php

namespace Drupal\sg_batch_plugin\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a batch worker plugin item annotation object.
 *
 * @see \Drupal\sg_batch_plugin\BatchWorkerPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class BatchWorker extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the batch task.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

}
