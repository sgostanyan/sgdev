<?php

namespace Drupal\sg_batch_plugin\Plugin\BatchWorker;

use Drupal\sg_batch_plugin\BatchWorkerCallbackPluginInterface;
use Drupal\sg_batch_plugin\BatchWorkerPluginBase;

/**
 * Provides a 'default' batch worker.
 *
 * @BatchWorker (
 *   id = "default",
 *   name = @Translation("Default")
 * )
 */
class DefaultBatchWorker extends BatchWorkerPluginBase implements BatchWorkerCallbackPluginInterface {

  /**
   * @inheritDoc
   */
  public static function execute($params, &$context) {

    $node = \Drupal::entityTypeManager()->getStorage('node')->load($params['nid']);

    $context['message'] = 'Processing : ' . $node->label();
    $context['results'][] = $node->label();
  }

  /**
   * @inheritDoc
   */
  public static function finished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(count($results),
        'One task processed.',
        '@count tasks processed.');
      \Drupal::messenger()->addMessage($message);
    }
    else {
      \Drupal::messenger()->addError(t('Finished with an error.'));
    }
  }

}
