<?php

namespace Drupal\sg_entity_service\Manager;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class SgFileManager.
 *
 * @package Drupal\sg_entity_service\Manager
 */
class SgFileManager {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * SgFileManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   * @param \Drupal\file\FileRepositoryInterface $fileRepository
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   */
  public function __construct(EntityTypeManager $entityTypeManager, FileSystemInterface $fileSystem, FileUrlGeneratorInterface $fileUrlGenerator, FileRepositoryInterface $fileRepository, RequestStack $requestStack) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->fileRepository = $fileRepository;
    $this->requestStack = $requestStack;
  }

  /**
   * @param $source
   * @param $filename
   * @param $destination
   *
   * @return int|string|null
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generateFileEntity($source, $filename, $destination = 'public://') {

    if (file_exists($source . $filename)) {
      // Get file.
      $filepath = $source . $filename;
      $data = file_get_contents($filepath);

      // Prepare destination.
      if ($data) {
        if ($this->fileSystem->prepareDirectory($destination, $this->fileSystem::CREATE_DIRECTORY)) {
          $file = $this->fileRepository->writeData($data, $destination . $filename, $this->fileSystem::EXISTS_REPLACE);
          if ($file) {
            return $file->id();
          }
        }
      }
    }
    return NULL;
  }

  /**
   * @param $fid
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getFileInfos($fid) {
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if ($file) {
      $size = $this->sanitizeFileSize($file->getSize());
      $mimeArray = explode('/', $file->getMimeType());
      $fileMime = end($mimeArray);
      $urls = $this->getFileUrl($fid);

      return [
        'mime' => $fileMime,
        'size' => $size,
        'url' => [
          'absolute' => isset($urls['absolute']) ? $urls['absolute'] : '',
          'relative' => isset($urls['relative']) ? $urls['relative'] : '',
          'internal' => isset($urls['internal']) ? $urls['internal'] : '',
          'system' => isset($urls['system']) ? $urls['system'] : '',
        ],
        'referenced' => file_get_file_references($file),
      ];
    }
  }

  /**
   * @param $size
   *
   * @return int|string
   */
  public function sanitizeFileSize($size) {
    $fileSize = intval($size);
    $ko = $fileSize / 1000;
    if ($ko < 1000) {
      $fileSize = round($ko) . t(" Ko");
    }
    else {
      $fileSize = round($ko / 1000, 2) . t(" Mo");
    }
    return $fileSize;
  }

  /**
   * @param $fid
   * @param array $options
   *
   * @return array|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFileUrl($fid, array $options = []) {
    if ($fid) {
      $file = $this->entityTypeManager->getStorage('file')->load($fid);
      $path = $file->getFileUri();
      if ($file && $path) {
        return [
          'absolute' => $this->fileUrlGenerator->generateAbsoluteString($path),
          'relative' => str_replace($this->requestStack->getCurrentRequest()->getSchemeAndHttpHost(), '', $this->fileUrlGenerator->generateAbsoluteString($path)),
          'internal' => $path,
          'system' => $this->fileSystem->realpath($path),
        ];
      }
    }
    return NULL;
  }

}
