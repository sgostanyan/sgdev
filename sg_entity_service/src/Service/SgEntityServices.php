<?php

namespace Drupal\sg_entity_service\Service;

use Drupal\sg_entity_service\Manager\SgEntityDisplayManager;
use Drupal\sg_entity_service\Manager\SgEntityStorageManager;
use Drupal\sg_entity_service\Manager\SgFileManager;
use Drupal\sg_entity_service\Manager\SgImageManager;

/**
 * Class SgEntityServices
 *
 * @package Drupal\sg_entity_service\Service
 */
class SgEntityServices {

  /**
   * @var \Drupal\sg_entity_service\Manager\SgEntityStorageManager
   */
  protected $sgEntityStorageManager;

  /**
   * @var \Drupal\sg_entity_service\Manager\SgEntityDisplayManager
   */
  protected $sgEntityDisplayManager;

  /**
   * @var \Drupal\sg_entity_service\Manager\SgFileManager
   */
  protected $sgFileManager;

  /**
   * @var \Drupal\sg_entity_service\Manager\SgImageManager
   */
  protected $sgImageManager;

  /**
   * SgEntityServices constructor.
   *
   * @param \Drupal\sg_entity_service\Manager\SgEntityStorageManager $sgEntityStorageManager
   * @param \Drupal\sg_entity_service\Manager\SgEntityDisplayManager $sgEntityDisplayManager
   * @param \Drupal\sg_entity_service\Manager\SgFileManager $sgFileManager
   * @param \Drupal\sg_entity_service\Manager\SgImageManager $sgImageManager
   */
  public function __construct(SgEntityStorageManager $sgEntityStorageManager, SgEntityDisplayManager $sgEntityDisplayManager, SgFileManager $sgFileManager, SgImageManager $sgImageManager) {
    $this->sgEntityStorageManager = $sgEntityStorageManager;
    $this->sgEntityDisplayManager = $sgEntityDisplayManager;
    $this->sgFileManager = $sgFileManager;
    $this->sgImageManager = $sgImageManager;
  }

  /**
   * @return \Drupal\sg_entity_service\Manager\SgEntityStorageManager
   */
  public function getEntityStorageManager() {
    return $this->sgEntityStorageManager;
  }

  /**
   * @return \Drupal\sg_entity_service\Manager\SgEntityDisplayManager
   */
  public function getEntityDisplayManager() {
    return $this->sgEntityDisplayManager;
  }

  /**
   * @return \Drupal\sg_entity_service\Manager\SgFileManager
   */
  public function getFileManager() {
    return $this->sgFileManager;
  }

  /**
   * @return \Drupal\sg_entity_service\Manager\SgImageManager
   */
  public function getImageManager() {
    return $this->sgImageManager;
  }

}
