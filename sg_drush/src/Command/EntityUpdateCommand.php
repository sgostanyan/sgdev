<?php

namespace Drupal\sg_drush\Command;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Class EntityUpdateCommand
 *
 * @package Drupal\sg_drush\Command
 */
class EntityUpdateCommand extends DrushCommands {

  /**
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManager
   */
  protected EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager;

  /**
   * EntityUpdateCommand constructor.
   *
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManager $entityDefinitionUpdateManager
   */
  public function __construct(EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager) {
    parent::__construct();
    $this->entityDefinitionUpdateManager = $entityDefinitionUpdateManager;
  }

  /**
   * entity definition update
   *
   * @command sg:entity-update
   * @aliases sg:entup,
   * @usage drush sg:entity-update
   * @usage drush sg:entup
   *
   */
  public function entityUpdate(): void {
    foreach ($this->entityDefinitionUpdateManager->getChangeList() as $item) {
      $this->writeln($item->getName());
      $item->update();
    }
  }

}