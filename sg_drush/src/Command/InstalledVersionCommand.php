<?php

namespace Drupal\sg_drush\Command;

use Drupal\Core\Update\UpdateHookRegistry;
use Drush\Commands\DrushCommands;

/**
 * Class InstalledVersion
 *
 * @package Drupal\sgdev_drush\Command
 */
class InstalledVersionCommand extends DrushCommands {

  /**
   * @var \Drupal\Core\Update\UpdateHookRegistry
   */
  protected UpdateHookRegistry $updateHookRegistry;

  /**
   * InstalledVersionCommand constructor.
   *
   * @param \Drupal\Core\Update\UpdateHookRegistry $updateHookRegistry
   */
  public function __construct(UpdateHookRegistry $updateHookRegistry) {
    parent::__construct();
    $this->updateHookRegistry = $updateHookRegistry;
  }

  /**
   * Set hook update version
   *
   * @command sg:installed-version:set
   * @aliases sg:iv:set,
   * @usage drush sg:installed-version:set "module_name" "version"
   * @usage drush sg:iv:set "module_name" "version"
   *
   */
  public function setVersion(string $moduleName, int $version) {
    $this->updateHookRegistry->setInstalledVersion($moduleName, $version);
  }

  /**
   * Get hook update version
   *
   * @command sg:installed-version:get
   * @aliases sg:iv:get,
   * @usage drush sg:installed-version:get "module_name"
   * @usage drush sg:iv:get "module_name"
   *
   */
  public function getVersion($moduleName) {
    $this->logger()->notice($this->updateHookRegistry->getInstalledVersion($moduleName));
  }


}