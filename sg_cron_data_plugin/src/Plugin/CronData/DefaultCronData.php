<?php

namespace Drupal\sg_cron_data_plugin\Plugin\CronData;

use Drupal\sg_cron_data_plugin\CronDataPluginBase;

/**
 * Provides a 'default' cron plugin base.
 *
 * @CronData(
 *   id = "default_cron",
 *   interval = "86400",
 *   preferred_hour = "00",
 *   name = @Translation("My Cron"),
 * )
 */
class DefaultCronData extends CronDataPluginBase {

}
