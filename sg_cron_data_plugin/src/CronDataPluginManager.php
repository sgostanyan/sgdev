<?php

namespace Drupal\sg_cron_data_plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class CronDataPluginManager
 *
 * @package Drupal\sg_cron_data_plugin
 */
class CronDataPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/CronData', $namespaces, $module_handler, 'Drupal\sg_cron_data_plugin\CronDataPluginInterface', 'Drupal\sg_cron_data_plugin\Annotation\CronData');
    $this->alterInfo('cron_data_info');
    $this->setCacheBackend($cache_backend, 'cron_data_plugins');
  }

}
