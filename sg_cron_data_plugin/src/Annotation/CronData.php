<?php

namespace Drupal\sg_cron_data_plugin\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Cron data item annotation object.
 *
 * @see \Drupal\sg_cron_data_plugin\CronCheckerPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class CronData extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The plugin name
   *
   * @var string
   */
  public string $name;

  /**
   * The plugin state storage id
   *
   * @var string
   */
  public string $stateId;

  /**
   * The plugin cron interval
   *
   * @var string
   */
  public string $interval;

  /**
   * The plugin cron preferred run hour
   *
   * @var string
   */
  public string $preferredHour;

}
