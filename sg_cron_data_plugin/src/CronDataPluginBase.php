<?php

namespace Drupal\sg_cron_data_plugin;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a cron plugin base.
 *
 * id => plugin id
 * interval => time interval in second
 * preferred_hour => hour of day (24h format). Leave empty string if not needed
 *
 * @Cron(
 *   id = "MyCron",
 *   interval = "",
 *   preferred_hour = "",
 *   name = @Translation("My Cron"),
 * )
 */
class CronDataPluginBase extends PluginBase implements CronDataPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * CronManagerPluginBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\State\StateInterface $state
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StateInterface $state) {
    $this->state = $state;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   *
   * @return \Drupal\sg_cron_data_plugin\CronDataPluginBase|static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('state'));
  }

  /**
   * @return bool
   * @throws \Exception
   */
  public function isValid(): bool {

    // Get date time infos.
    $dateTimeData = $this->getDateTimeData();
    $currentTime = $dateTimeData['currentTimestamp'];
    $currentHour = $dateTimeData['currentHour'];
    $lastExecutedTime = $dateTimeData['lastExecutedTime'];

    $status = FALSE;

    switch (TRUE) {
      case empty($lastExecutedTime):
      case $currentTime - $lastExecutedTime >= $this->getInterval():

        // Check if there is a preferred hour for running task
        if (!empty($this->getPreferredHour()) && $this->getPreferredHour() != $currentHour) {
          // Not the time yet
          break;
        }

        // Store datetime
        $this->setDateTimeState();
        $status = TRUE;
        break;
    }
    return $status;
  }

  /**
   * @return array
   * @throws \Exception
   */
  protected function getDateTimeData(): array {
    $dateTime = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
    return [
      'currentTimestamp' => $dateTime->getTimestamp(),
      'currentHour' => $dateTime->format('H'),
      'lastExecutedTime' => $this->state->get($this->getStateId()),
    ];
  }

  /**
   * @return string
   */
  function getStateId(): string {
    return sprintf('sg_cron_data.%s', $this->getPluginId());
  }

  /**
   * @return string
   */
  function getInterval(): string {
    return $this->pluginDefinition['interval'];
  }

  /**
   * @return string
   */
  function getPreferredHour(): string {
    return $this->pluginDefinition['preferred_hour'];
  }

  /**
   * @return void
   * @throws \Exception
   */
  public function setDateTimeState() {
    $dateTimeData = $this->getDateTimeData();
    $this->state->set($this->getStateId(), $dateTimeData['currentTimestamp']);
  }

  /**
   * @return string
   */
  function getName(): string {
    return $this->pluginDefinition['name'];
  }
}
