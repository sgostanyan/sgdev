<?php

namespace Drupal\sg_cron_data_plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface CronDataPluginInterface
 *
 * @package Drupal\sg_cron_data_plugin
 */
interface CronDataPluginInterface extends PluginInspectionInterface {

  /**
   * @return string
   */
  function getName(): string;

  /**
   * @return string
   */
  function getStateId(): string;

  /**
   * @return string
   */
  function getInterval(): string;

  /**
   * @return string
   */
  function getPreferredHour(): string;

  /**
   * @return bool
   */
  function isValid(): bool;
}
