# Example

## Cron plugin

```php
  // Instantiate a new cron data plugin manager
  $cronPluginManager = \Drupal::service('plugin.manager.cron_data_plugin');

  // Create default cron data instance
  $cronPluginManagerInstance = $cronPluginManager->createInstance('default_cron');
  
  // Check if cron is valid
  $isValid = $cronPluginManagerInstance->isValid();
```
