<?php

namespace Drupal\import_manager\Plugin\BatchTask;

/**
 * Provides a 'migrate manager Migrate' batch task.
 *
 * @BatchTask (
 *   id = "eventMigration",
 *   name = @Translation("Event Migrate")
 * )
 */
class EventMigrateBatchTask extends MigrateBatchTaskBase {

}
