<?php

namespace Drupal\import_manager\Plugin\ImportRunner;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\import_manager\ImportRunnerPluginBase;

/**
 * Plugin implementation of the import_runner.
 *
 * @ImportRunner(
 *   id = "event",
 *   label = @Translation("Event"),
 *   description = @Translation("Event description."),
 *   sourceBatchId = "prepareEventDataSource",
 *   migrationBatchId = "eventMigration",
 *   cronId = "",
 *   migrateIds = {
 *       "test_article_node_csv_import",
 *       "test_article_node_csv_import"
 *   },
 *   filename = "test_article",
 *   directory = "private://import/",
 * )
 */
class EventRunner extends ImportRunnerPluginBase implements ContainerFactoryPluginInterface {

  const MANDATORY_PLUGINS = [
    'sourceBatchId',
    'migrationBatchId',
    'migrateIds',
  ];

  /**
   * @return array
   */
  public static function getDataSource() {
    /* Prepare source */
    $source = simplexml_load_string(file_get_contents('https://madeinfoot.ouest-france.fr/flux/rss_ligue.php?id=16'));

    // Arrange data.
    foreach ($source->channel->item as $item) {
      $items[] = [$item->title->__toString(), $item->description->__toString()];
    }
    return $items;
  }

}
