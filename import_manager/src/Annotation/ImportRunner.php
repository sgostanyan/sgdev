<?php

namespace Drupal\import_manager\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines import_runner annotation object.
 *
 * @Annotation
 */
class ImportRunner extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The id of the BatchTask plugin related to the source of data.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $sourceBatchId;

  /**
   * The id of the BatchTask plugin related to the migration process.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $migrationBatchId;

  /**
   * The id of the Cron plugin related to the migration process.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $cronId;

  /**
   * The ids of the migrate config that need to be executed.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $migrateIds;

  /**
   * The generated source CSV filename without extension.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $filename;

  /**
   * The directory to save the CSV file.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $directory;

}
