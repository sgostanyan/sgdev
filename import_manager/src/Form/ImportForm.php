<?php

namespace Drupal\import_manager\Form;

use Drupal\batch_factory\BatchFactoryPluginManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\import_manager\ImportRunnerPluginManager;
use Drupal\import_manager\Plugin\ImportRunner\EventRunner;
use Drupal\import_manager\Service\ImportManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImportForm.
 */
class ImportForm extends FormBase {

  /**
   * Drupal\import_manager\Service\ImportManagerInterface definition.
   *
   * @var \Drupal\import_manager\Service\ImportManagerInterface
   */
  protected ImportManagerInterface $importManager;

  /**
   * @var \Drupal\import_manager\ImportRunnerPluginManager
   */
  protected ImportRunnerPluginManager $importRunnerPluginManager;

  /**
   * @var \Drupal\batch_factory\BatchFactoryPluginManager
   */
  protected BatchFactoryPluginManager $batchFactoryPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->importManager = $container->get('sgdev.import_manager.manager');
    $instance->importRunnerPluginManager = $container->get('plugin.manager.import_runner');
    $instance->batchFactoryPluginManager = $container->get('plugin.manager.batch_factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['run'] = [
      '#type' => 'select',
      '#title' => $this->t('Update source for :'),
      '#options' => $this->getPluginList(),
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->execute(explode(':', $form_state->getValue('run')));
  }

  /**
   * @param $run
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function execute($run) {
    /** @var \Drupal\import_manager\ImportRunnerPluginBase $importRunnerInstance */
    $importRunnerInstance = $this->importRunnerPluginManager->createInstance($run[0]);

    switch (strpos($run[1], 'prepare') !== FALSE ? '0' : '1') {
      case '0':
        strpos($run[1], 'Batch') !== FALSE ? $importRunnerInstance->{$run[1]}(EventRunner::getDataSource(), NULL, NULL, FALSE) : $importRunnerInstance->{$run[1]}(EventRunner::getDataSource(),
          NULL,
          NULL);
        break;
      case '1':
        strpos($run[1], 'Batch') !== FALSE ? $importRunnerInstance->{$run[1]}(TRUE, TRUE, FALSE) : $importRunnerInstance->{$run[1]}(TRUE, TRUE);
        break;
    }
  }

  /**
   * @return array
   */
  protected function getPluginList() {
    $runner = [];
    foreach ($this->importRunnerPluginManager->getDefinitions() as $id => $definitions) {
      $runner[$definitions['label']->__toString()] = [
        $id . ":prepareDataSource" => $this->t("Prepare source"),
        $id . ":prepareBatchDataSource" => $this->t("Prepare source batch"),
        $id . ":runMigration" => $this->t("Run import"),
        $id . ":runBatchMigration" => $this->t("Run import batch"),
      ];
    }
    return $runner;
  }

}
