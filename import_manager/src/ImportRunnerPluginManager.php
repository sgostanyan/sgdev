<?php

namespace Drupal\import_manager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * ImportRunner plugin manager.
 */
class ImportRunnerPluginManager extends DefaultPluginManager {

  /**
   * Constructs ImportRunnerPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ImportRunner',
      $namespaces,
      $module_handler,
      'Drupal\import_manager\ImportRunnerInterface',
      'Drupal\import_manager\Annotation\ImportRunner'
    );
    $this->alterInfo('import_runner_info');
    $this->setCacheBackend($cache_backend, 'import_runner_plugins');
  }

}
