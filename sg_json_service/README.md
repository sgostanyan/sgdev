# Example

## Instantiate a json service

```php
// Json manager
$jsonManager = \Drupal::service('sgdev.sg_json_service.manager');

```

## Usage

```php

$users = Drupal::entityTypeManager()->getStorage('user')->loadMultiple();

/**
 * @var \Drupal\user\Entity\User $user
 */
foreach ($users as $user) {

  $userDataItems = [
    $user->id(),
    $user->getAccountName(),
    $user->getEmail(),
  ];

  /**
   * @var \Drupal\sg_json_service\Service\CsvManagerInterface $jsonManager
   */
  $jsonManager->writeJsonFile([$userDataItems], 'fdtest', 'private://import', 1);
    
}
```

