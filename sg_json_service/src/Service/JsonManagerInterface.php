<?php

namespace Drupal\sg_json_service\Service;


/**
 * Interface JsonManagerInterface
 *
 * @package Drupal\sg_json_service\Service
 */
interface JsonManagerInterface {

  /**
   * @param array $data
   * @param string $filename
   * @param string $directory
   * @param bool $append
   *
   * @return bool
   */
  public function writeJsonFile(array $data, string $filename, string $directory, bool $append = FALSE): bool;
}
