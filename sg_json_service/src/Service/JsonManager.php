<?php

namespace Drupal\sg_json_service\Service;

use Drupal\Core\File\FileSystemInterface;

/**
 * Class JsonManager
 *
 * @package Drupal\sg_json_service\Service
 */
class JsonManager implements JsonManagerInterface {

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * CsvManager constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * @param array $data
   * @param string $filename
   * @param string $directory
   * @param bool $append
   *
   * @return bool
   */
  public function writeJsonFile(array $data, string $filename, string $directory, bool $append = FALSE): bool {

    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::EXISTS_REPLACE);
    $filePath = $directory . '/' . $filename . '.json';

    if (!$append && file_exists($filePath)) {
      $this->fileSystem->delete($filePath);
      $this->fileSystem->saveData('', $filePath);
    }

    $fileContent = file_get_contents($filename);
    $fileData = json_decode($fileContent, TRUE);
    if (!is_array($fileData) || array_key_exists('data', $fileData)) {
      $fileData['data'][] = $data;
    }
    return !empty(file_put_contents($filename, json_encode($fileData)));
  }

}
